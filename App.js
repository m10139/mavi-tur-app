/* eslint-disable react-native/no-inline-styles */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {Component, useState, useEffect} from 'react';
import DatePicker from 'react-native-modern-datepicker';

import {
  Text,
  StyleSheet,
  Image,
  View,
  Button,
  Alert,
  Linking,
  TouchableOpacity,
  ScrollView,
  ImageBackground,
} from 'react-native';

const App = () => {
  const [cost, setCost] = useState(0);
  const [cost2, setCost2] = useState(0);
  const [cost3, setCost3] = useState(0);

  const add = () => {
    setCost(cost + 500);
  };

  const add2 = () => {
    setCost(cost + 1000);
  };

  const add3 = () => {
    setCost(cost + 250);
  };

  const BasicUsage = () => {
    const [selectedDate, setSelectedDate] = useState('');

    return <DatePicker onSelectedChange={date => setSelectedDate(date)} />;
  };

  useEffect(() => {
    setCost(Math.trunc(0));
  }, []);

  return (
    <View style={{flex: 1}}>
      <Text style={styles.titleBackground}>
        <Text style={styles.title}> Mavi Tur </Text>
      </Text>
      <Image
        source={{
          uri: 'https://www.milaidhoo.com/ja/wp-content/uploads/sites/14/2021/12/Milaidhoo_Maldives_Batheli_By_The_Reef__10_-1920.jpg',
        }}
        style={{height: 200, width: 500}}
      />
      <Button
        title="🌴 Information on new working hours for the holidays !"
        color="mediumturquoise"
        onPress={() =>
          Alert.alert(
            'Our office will only be open to visit from 11 am to 3 pm for holidays.',
          )
        }
      />
      <ScrollView decelerationRate={0.5}>
        <TouchableOpacity
          style={{margin: 10}}
          onPress={() => {
            Linking.openURL(
              'https://traveltriangle.com/blog/maldives-travel-tips/',
            );
          }}>
          <Text
            style={{
              alignSelf: 'center',
              color: 'darkblue',
              fontSize: 25,
              textAlign: 'center',
              fontFamily: 'HomemadeApple-Regular',
            }}>
            Click here to get tips on your Maldives Trip!
          </Text>
        </TouchableOpacity>
        <Text style={styles.textBlock}>
          The Maldives is a breathtaking tourist destination with white sand
          beaches, turquoise oceans, magnificent islands, and awe-inspiring
          marine life. It attracts all sorts of tourists, including solitary
          travelers, honeymooners, and families, who want to enjoy a relaxing
          holiday amidst the enormous Indian Ocean. With so many alternatives
          and so little time, deciding what to do and what to avoid becomes a
          difficult issue. As a result, here's a customized travel plan for your
          next trip to the Maldives, with Maldives honeymoon package, Maldives
          tour packages and Maldives holiday packages for a stress-free and
          relaxing vacation.
        </Text>
        <Image
          source={{
            uri: 'https://www.planetware.com/wpimages/2021/07/maldives-best-all-inclusive-resorts-hurawalhi-island-resort-villa.jpg',
          }}
          style={{height: 200, width: 500}}
        />
        <Button title="Travel Pack Family - Add to Cart" onPress={add2} />
        <Image
          source={{
            uri: 'https://media-cdn.tripadvisor.com/media/photo-s/1b/f4/64/09/aerial-view.jpg',
          }}
          style={{height: 200, width: 500}}
        />
        <Button title="Travel Pack Small - Add to Cart" onPress={add} />
        <Image
          source={{
            uri: 'https://pix10.agoda.net/hotelImages/7458162/0/b516a92cb50c969a0834002737a4b068.jpg?ca=23&ce=0&s=1024x768',
          }}
          style={{height: 200, width: 500}}
        />

        <Button title="Extra Amenities - Add to Cart" onPress={add3} />
        <Text style={styles.cost}> Cost of your Trip: {cost + cost2} </Text>
        <DatePicker
          options={{
            backgroundColor: 'turquoise',
            textHeaderColor: 'blue',
            textDefaultColor: 'black',
            selectedTextColor: '#fff',
            mainColor: '#2575FC',
            textSecondaryColor: 'steelblue',
            borderColor: 'rgba(122, 146, 165, 0.1)',
          }}
          current="2022-03-31"
          selected="2022-03-31"
          mode="calendar"
          minuteInterval={30}
          style={{borderRadius: 10}}
        />
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  title: {
    marginTop: 32,
    paddingHorizontal: 24,
    color: '#4829ef',
    fontSize: 40,
    textAlign: 'center',
    fontFamily: 'HomemadeApple-Regular',
  },
  titleBackground: {
    textAlign: 'center',
    fontSize: 15,
    backgroundColor: 'turquoise',
  },
  textBlock: {
    backgroundColor: 'lightsteelblue',
    color: 'black',
    margin: 5,
    padding: 10,
    fontSize: 20,
    textAlign: 'justify',
    fontFamily: 'Courgette-Regular',
  },
  cost: {
    marginTop: 20,
    marginBottom: 20,
    width: '100%',
    fontSize: 20,
    textAlign: 'center',
    color: 'black',
    fontFamily: 'Courgette-Regular',
    backgroundColor: 'lightgreen',
  },
});

export default App;
